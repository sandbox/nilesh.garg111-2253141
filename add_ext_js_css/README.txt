INTRODUCTION
------------

 - This Module aims to add external JSS and CSS in drupal way and also to
    modify which JS or CSS file to add from admin panel.
 - Can restrict the theme to add JS and CSS externally that are mentioned in the form.

CONFIGURATION
-------------
 
 - Add the name of external JS and CSS file to add in the textboxes
   (one entry each line).
 - Add the name of the theme in which you don't want to add the files in the
   text box labeled "Exclude External Addition in which Theme".