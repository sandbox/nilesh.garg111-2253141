<?php

/**
 * @file
 * Admin page callbacks for the Add external JS or CSS Form.
 */

/**
 * Form to config  Add external JS or CSS Form.
 */
function _add_ext_js_css_admin() {
  $form = array();

  $form['add_ext_js'] = array(
    '#type' => 'textarea',
    '#title' => t('External JS Files to add'),
    '#default_value' => _add_ext_js_css_admin_convert_array_to_text(variable_get('ext_js_value', array())),
    '#description' => t('Add JS file name (only external), one per line.'),
  );
  
  $form['add_ext_css'] = array(
    '#type' => 'textarea',
    '#title' => t('External CSS Files to add'),
    '#default_value' => _add_ext_js_css_admin_convert_array_to_text(variable_get('ext_css_value', array())),
    '#description' => t('Add CSS file name (only external), one per line.'),
  );
  
   $form['ext_excluded_theme_name'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude External Addition in which Theme'),
    '#default_value' => _add_ext_js_css_admin_convert_array_to_text(variable_get('ext_excluded_theme_value', array())),
    '#description' => t('Add Theme Machine name one per line.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}


/**
 * Submit handler for Add external JS or CSS Form.
 * Saves external js or css links to call.
 */
function _add_ext_js_css_admin_submit($form, &$form_state) {
  //Setting the variable after changing it to array
  variable_set('ext_js_value', _add_ext_js_css_admin_convert_text_to_array($form_state['values']['add_ext_js']));
  variable_set('ext_css_value', _add_ext_js_css_admin_convert_text_to_array($form_state['values']['add_ext_css']));
  variable_set('ext_excluded_theme_value', _add_ext_js_css_admin_convert_text_to_array($form_state['values']['ext_excluded_theme_name']));
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Converts a string representation of external JS or CSS links to an array.
 *
 * @param $text_to_convert
 *   A string representation of external JS or CSS links.
 *
 * @return
 *   An array representation of external JS or CSS links.
 */
function _add_ext_js_css_admin_convert_text_to_array($text_to_convert) {
  $text_to_convert = preg_split("/(\r\n|\r|\n)/", $text_to_convert, NULL, PREG_SPLIT_NO_EMPTY);
  return $text_to_convert;
}

/**
 * Converts an array representation of external JS or CSS links to a string.
 *
 * @param $array_to_convert
 *   An array representation of external JS or CSS links.
 *
 * @return
 *   A string representation of external JS or CSS links.
 */
function _add_ext_js_css_admin_convert_array_to_text($array_to_convert) {
  return implode("\n", $array_to_convert);
}
